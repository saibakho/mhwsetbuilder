import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
	Header,
	//createAppContainer,
	createStackNavigator,
	createDrawerNavigator,
} from "react-navigation"
import SearchBar from "./components/SearchBar"
import MainScreen from "./screens/MainScreen"
import SearchScreen from "./screens/SearchScreen"


/*export default App = () => {
	return (
		<MainScreen/>
	)
}*/

const ScreenNavigator = createStackNavigator({
	Main: {
		navigationOptions: {
			title: "MHW配裝模擬器",
		},
		screen: MainScreen
	},
	Search: {
		navigationOptions: {
			//title: "Search",
			headerTitle: <SearchBar/>
		},
		screen: SearchScreen
	},
})

export default ScreenNavigator
