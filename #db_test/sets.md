# 配裝器使用記錄

## 屬性弓 - 火龍奧秘配裝

### 火屬性

部位|攻擊/防禦|名稱|屬性/效果/技能|鑲嵌槽|裝飾品
---|---|---|---|---|---
武器| 240 | 蠻顎弓        | 火390      | 1-1--
頭  |  92 | 帝王 頭盔 γ   | 看破+2     | 2-2--
身  |  86 | 火龍 鎧甲 β   | 弱特+2     | 1----
腕  |  92 | 帝王 腕甲 γ   | 看破+3     | 3----
腰  |  86 | 火龍 腰甲 β   | 火屬強化+2 | 2----
腳  |  92 | 熔山龍 護腿 γ | 看破+1     | 3-2--
護石|  -  | 體術護石III   | 爆破強化+2 | -----
合計| 448 | 
