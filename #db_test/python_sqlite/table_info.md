# MHW.db Table Info


- item
- language
- monster

- skilltree

id|max_level|icon_color
:---:|:---:|:---:
integer|integer|text
134|1|violet

- weapon_ammo
- weapon_melody
- charm
- kinsect
- item_text
- item_combination
- location_text
- monster_text
- monster_hitzone
- monster_break
- monster_reward_condition_text

- skilltree_text

id|lang_id|name|description
:---:|:---:|:---:|:---:
integer|text|text|text
134|"zh"|"毒瓶追加"|"裝備弓時，可裝備毒瓶。"

- skill

skilltree_id|lang_id|level|description
:---:|:---:|:---:|:---:
integer|text|integer|text
134|"zh"|1|"變成可裝備毒瓶。"

- armorset

id|rank|monster_id|armorset_bonus_id
:---:|:---:|:---:|:---:
integer|text|integer|integer
169|HR|50|26

- armorset_bonus_text

id|lang_id|name|description
:---:|:---:|:---:|:---:
integer|integer|text|text
26|"zh"|"龍騎士之證"|None

- armorset_bonus_skill

setbonus_id|skilltree_id|required
:---:|:---:|:---:
integer|integer|integer
26|120|4

- weapon
- weapon_melody_text
- decoration
- charm_skill
- charm_recipe
- charm_text
- kinsect_text
- location_item
- location_camp_text
- monster_habitat
- monster_hitzone_text
- monster_break_text
- monster_reward

- armorset_text

id|lang_id|name
:---:|:---:|:---:
integer|text|text
132|"zh"|"帝王β"
173|"zh"|"搖曳鰻頭套α"

- armor

id|order_id|rarity|rank|armor_type|armorset_id|armorset_bonus_id|male|female|slot_1|slot_2|slot_3|defense_base|defense_max|defense_augment_max|fire|water|thunder|ice|dragon
:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:

- weapon_text
- weapon_recipe
- weapon_skill
- decoration_text
- kinsect_recipe

- armor_text

id|lang_id|name
:---:|:---:|:---:
integer|text|text
692|"zh"|"搖曳鰻頭套α"

- armor_skill

armor_id|skilltree_id|level
:---:|:---:|:---:
integer|integer|integer
692|72|2

- armor_recipe
