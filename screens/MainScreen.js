import React from 'react'
import {
	Text,
	View,
	Image,
	StyleSheet,
	ScrollView,
	TouchableOpacity,
} from 'react-native'
import DrawerLayout from "react-native-gesture-handler/DrawerLayout"

import { Refs } from "../components/Global"
import ItemRow from "../components/ItemRow"
import DBManager from "../components/DBManager"
import TextContainer from "../components/TextContainer"
import DrawerRenderView from "../components/DrawerRenderView"

export default class MainScreen extends React.Component {
	/* replace global.refs with params passing ? still considering...
	constructor(props) {
		super(props)
		let template = {	
			slots: "0-0-0",
			rarity: "?",
			defense: {max: null, augMax: null},
			itemName: null,
			itemSkill: [{name: null, level: null}, {name: null, level: null}],				
		},
		this.state = {
			infos: {
				head: template,
				chest:
				...
			},
			currentSlots: {
				head: 
				chest:
				...
			}
		}
	}*/
	render() {
		let { navigation } = this.props		
		return (
			<DrawerLayout
				drawerWidth={/*180*/210}
				drawerType="slide"
				overlayColor="gray"
				drawerPosition={DrawerLayout.positions.Right}
				renderNavigationView={() => <DrawerRenderView ref={Refs.drawer}/>}>
				<View style={styles.container}>
					<ItemRow part="weapon" ref={Refs.weapon} navigation={navigation}/>
					<View style={styles.divider}/>

					<ItemRow part="head" ref={Refs.head} navigation={navigation}/>
					<View style={styles.divider}/>
					
					<ItemRow part="chest" ref={Refs.chest} navigation={navigation}/>
					<View style={styles.divider}/>
					
					<ItemRow part="arm" ref={Refs.arm} navigation={navigation}/>
					<View style={styles.divider}/>
					
					<ItemRow part="waist" ref={Refs.waist} navigation={navigation}/>
					<View style={styles.divider}/>
					
					<ItemRow part="leg" ref={Refs.leg} navigation={navigation}/>
					<View style={styles.divider}/>

					<ItemRow part="charm" ref={Refs.charm} navigation={navigation}/>
				</View>
			</DrawerLayout>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: "column",
	},
	divider: {
		height: 1,
		marginHorizontal: 5,
		backgroundColor: "gray",
	},
});
