import React from 'react'
import {
	Text,
	View,
	Image,
	StatusBar,
	StyleSheet,
	ScrollView,
} from 'react-native'
import ItemRow from "../components/ItemRow"
import TextContainer from "../components/TextContainer"

export default class SearchScreen extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			part: this.props.navigation.getParam("part", null),
		}
	}
	
	render() {
		let { navigation } = this.props
		let { getParam } = navigation		
		let { part } = this.state
		//let { base, index, level } = navigation.state.params
		let example
		switch(part) {
			case "weapon":
				example = {					
					rarity: 8,
					slots: "0-0-0",
					defense: {max: null, augMax: null},
					itemName: "帝王金槍‧風飄",
					itemSkill: [{name: 391, level: "+10%"}, {name: "會心攻擊[屬性]", level: 390}],
					setSkill: {name: null, minLevel: null},
				};	break
			case "head":
				example = {
					rarity: 8,
					slots: "2-0-0",
					defense: {max: 78, augMax: 92},
					itemName: "麒麟角γ",
					itemSkill: [{name: "屬性異常耐性", level: 1}, {name: "雷屬性攻擊強化", level: 3}],
					setSkill: {name: "幻獸的恩寵", skills: [{name: "捕獲名人", minLevel: 3}]},
				};	break
			case "chest":
				example = {
					rarity: 8,
					slots: "1-1-0",
					defense: {max: 78, augMax: 92},
					itemName: "麒麟服飾γ",
					itemSkill: [{name: "屬性解放／裝填擴充", level: 2}, {name: null, level: null}],
					setSkill: {name: "幻獸的恩寵", skills: [{name: "捕獲名人", minLevel: 3}]},
				};	break
			case "arm":
				example = {
					rarity: 8,
					slots: "2-2-0",
					defense: {max: 78, augMax: 92},
					itemName: "麒麟長腕甲γ",
					itemSkill: [{name: "屬性異常耐性", level: 1}, {name: "屬性解放／裝填擴充", level: 1}],
					setSkill: {name: "幻獸的恩寵", skills: [{name: "捕獲名人", minLevel: 3}]},
				};	break
			case "waist":
				example = {
					rarity: 8,
					slots: "1-1-0",
					defense: {max: 78, augMax: 92},
					itemName: "麒麟腰環γ",
					itemSkill: [{name: "跑者", level: 2}, {name: null, level: null}],
					setSkill: {name: "幻獸的恩寵", skills: [{name: "捕獲名人", minLevel: 3}]},
				};	break
			case "leg":
				example = {
					rarity: 8,
					slots: "3-0-0",
					defense: {max: 78, augMax: 92},
					itemName: "麒麟護脛γ",
					itemSkill: [{name: "屬性異常耐性", level: 1}, {name: "雷屬性攻擊強化", level: 2}],
					setSkill: {name: "幻獸的恩寵", skills: [{name: "捕獲名人", minLevel: 3}]},
				};	break
			/*case "head":
				example = {					
					rarity: 8,
					slots: "1-1-1",
					defense: {max: 76, augMax: 90},
					itemName: "皇后琴弦γ",
					itemSkill: [{name: "無傷", level: 1}, {name: "廣域化", level: 2}],
					setSkill: {name: "炎妃龍的恩寵", skills: [{name: "突破耐力上限", minLevel: 2}, {name: "心眼／彈道強化", minLevel: 4}]},
				};	break
			case "chest":
				example = {
					rarity: 8,
					slots: "3-0-0",
					defense: {max: 76, augMax: 90},
					itemName: "皇后鎧甲γ",
					itemSkill: [{name: "耳塞", level: 2}, {name: "整備", level: 1}],
					setSkill: {name: "炎妃龍的恩寵", skills: [{name: "突破耐力上限", minLevel: 2}, {name: "心眼／彈道強化", minLevel: 4}]},
				};	break
			case "arm":
				example = {
					rarity: 8,
					slots: "3-1-0",
					defense: {max: 76, augMax: 90},
					itemName: "皇后腕甲γ",
					itemSkill: [{name: "挑戰者", level: 2}, {name: "迴避距離UP", level: 1}],
					setSkill: {name: "炎妃龍的恩寵", skills: [{name: "突破耐力上限", minLevel: 2}, {name: "心眼／彈道強化", minLevel: 4}]},
				};	break
			case "waist":
				example = {
					rarity: 8,
					slots: "3-0-0",
					defense: {max: 76, augMax: 90},
					itemName: "皇后腰甲γ",
					itemSkill: [{name: "體力增強", level: 2}, {name: "迴避性能", level: 2}],
					setSkill: {name: "炎妃龍的恩寵", skills: [{name: "突破耐力上限", minLevel: 2}, {name: "心眼／彈道強化", minLevel: 4}]},
				};	break
			case "leg":
				example = {
					rarity: 8,
					slots: "3-0-0",
					defense: {max: 76, augMax: 90},
					itemName: "皇后護腿γ",
					itemSkill: [{name: "耳塞", level: 2}, {name: "挑戰者", level: 1}],
					setSkill: {name: "炎妃龍的恩寵", skills: [{name: "突破耐力上限", minLevel: 2}, {name: "心眼／彈道強化", minLevel: 4}]},
				};	break*/
			case "charm":
				example = {
					rarity: 8,
					slots: "0-0-0",
					defense: {max: null, augMax: null},
					itemName: "匠之護石III",
					itemSkill: [{name: "匠", level: 3}, {name: null, level: null}],
					setSkill: {name: null, minLevel: null},
				};	break
			case "slot":
				example = {
					rarity: 8,
					slots: "0-0-0",
					defense: {max: null, augMax: null},
					itemName: "心眼珠",
					itemSkill: [{name: "心眼／彈道強化", level: 1}, {name: null, level: null}],
					setSkill: {name: null, minLevel: null},
				};	break
		}
		let slotInfo = {
			level: 2,

			base: getParam("base"),
			index: getParam("index"),
			slotLevel: getParam("slotLevel"),
		}
		return (
			<ScrollView style={styles.container}>
				<View style={styles.cell}>
					<ItemRow part={part} navigation={navigation} isRemove={true} slotInfo={slotInfo}/>
				</View>
				<View style={styles.divider}/>

				<View style={styles.cell}>
					<ItemRow part={part} navigation={navigation} itemInfo={example} slotInfo={slotInfo} slotLock={true}/>
				</View>
				<View style={styles.divider}/>

				<View style={styles.cell}>
					<ItemRow part={part} navigation={navigation} itemInfo={example} slotInfo={slotInfo} slotLock={true}/>
				</View>
				<View style={styles.divider}/>

				<View style={styles.cell}>
					<ItemRow part={part} navigation={navigation} itemInfo={example} slotInfo={slotInfo} slotLock={true}/>
				</View>
				<View style={styles.divider}/>

				<View style={styles.cell}>
					<TextContainer content={part ==="slot" ? "PART: "+part+" "+slotInfo.slotLevel : "PART: "+part}/>
				</View>
				<View style={styles.divider}/>
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: "column",
	},
	cell: {
		height: 72,
	},
	divider: {
		height: 1,
		marginHorizontal: 5,
		backgroundColor: "gray",
	},
});
