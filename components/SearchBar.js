import React from 'react'
import {
	Text,
	View,
	Image,
	TextInput,
	StyleSheet,
	TouchableOpacity,
} from 'react-native'
import TextContainer from "../components/TextContainer"

export default class SearchBar extends React.Component {
	render() {
		return (
			<View style={styles.container}>
				<TextInput style={styles.textInput} placeholder="裝備 / 技能 / 鑲嵌槽 (例: 3-1-0)"/>
				<TouchableOpacity style={styles.icon}>
					<Image style={styles.image} source={require("../assets/utils/search.png")}/>
				</TouchableOpacity>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: "row",
	},
	textInput: {
		flex: 1,
		height: 35,
		padding: 5,
		elevation: 2,
		borderWidth: 1,
		borderRadius: 5,
		borderColor: "gray",		
	},
	icon: {
		height: 35,
		aspectRatio: 1,
		alignItems: "center",
		justifyContent: "center",
		marginHorizontal: 10,
	},
	image: {
		width: "70%",
		height: undefined,
		aspectRatio: 1,
	},
});
