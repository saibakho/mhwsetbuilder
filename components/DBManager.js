import { SQLite } from "expo-sqlite"

export default class DBManager {
	constructor() {
		/*const DB_NAME
		const DB_blah*/

		this.db = SQLite.openDatabase({name: "mhw.db", createFromLocation: "~/www/mhw.db"})
	}

	selectRecords(query, callBack) {
		this.db.transaction(function(tx) {
			tx.executeSql(query, [], function(tx, results) {
				var records = []
				for (var row of results.rows)
					records.push(row)
				callBack(records)
			}, null);
		})
	}

	showTable() {
		this.selectRecords("SELECT * FROM armor_text;", function(records) {
			console.log("[armor_text]:")			
			for (var records of records) {
				console.log(records)
			}			
		})
	}

	getItemInfo(itemId) {
		this.selectRecords("SELECT * FROM armor_text WHERE id = "+itemId+" AND lang_id = 'zh';", (records) => {
			// callback
			console.log("Name: ")
			console.log(records)
		})
	}
}