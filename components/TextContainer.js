
import React from "react"
import {
	Text,
	View,
	StyleSheet,
} from "react-native"

export default class TextContainer extends React.Component {
	constructor(props) {
		super(props)
		this.styles = StyleSheet.create({
			textContainer: {
				flex: 1,
				alignItems: this.props.align,
				justifyContent: "center",
			},
			text: {
				//color: "white",
				fontFamily: "monospace",
			},
		})
	}

	render() {
		let { style, content, numberOfLines } = this.props
		return (
			<View style={style}>
				<View style={this.styles.textContainer}>
					<Text numberOfLines={numberOfLines} style={this.styles.text}>
						{content}
					</Text>
				</View>
			</View>
		)		
	}
}

TextContainer.defaultProps = {
	align: "center",	// "flex-start", "flex-end", "center", "stretch", "baseline"
	style: {
		flex: 1,
	},
	numberOfLines: 1,
}

