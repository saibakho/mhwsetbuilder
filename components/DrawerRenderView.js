import React from 'react'
import {
	Text,
	View,
	Image,
	StyleSheet,
	ScrollView,
	TouchableOpacity,
} from 'react-native'
import { Refs, LevelMax } from "../components/Global"
import TextContainer from "../components/TextContainer"

export default class DrawerRenderView extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			skillList: [],
			setSkillList: [],
		}
	}

	updateSkills() {
		let skillDict = {}
		let refs = {
			weapon: Refs.weapon, charm: Refs.charm,
			head: Refs.head, chest: Refs.chest, arm: Refs.arm, waist: Refs.waist, leg: Refs.leg,			
		}	// weird that it doesn't work without this object

		// set skill first
		let setSkillDict = {}
		for (let part in refs)
			if (part !== "weapon" && part !== "charm") {
				let { name, skills } = refs[part].current.state.itemInfo.setSkill
				if (name === null)
					continue
				if (name in setSkillDict) {
					setSkillDict[name].level += 1
					setSkillDict[name].parts.push(part)
				} else
					setSkillDict[name] = {name: name, level: 1, skills: skills, parts: [part]}
			}
		let setSkillList = []
		for (let name in setSkillDict)
			for (let skill of setSkillDict[name].skills)
				if (setSkillDict[name].level >= skill.minLevel)
					setSkillList.push({name: name, parts: setSkillDict[name].parts, skill: skill})
		this.setState({setSkillList: setSkillList})

		// then armor skill
		for (let part in refs) {
			let tempList = []	// push first whatever, the contents will be checked below (while making a list)
			if (part === "weapon")
				skillDict[refs.weapon.current.state.itemInfo.itemSkill[1].name] = 1
			else for (var i = 0; i < 2; i++)
				tempList.push(refs[part].current.state.itemInfo.itemSkill[i])
			for (var i = 0; i < 3; i++)
				tempList.push({name: refs[part].current.state.currentSlots[i].skill, level: 1})
			for (var i = 0; i < tempList.length; i++) {
				let { name, level } = tempList[i]
				if (name in skillDict)
					// cautious of LevelMax
					skillDict[name] += skillDict[name] < LevelMax[name] ? level : 0
				else
					skillDict[name] = level
			}
		}
		let isInSetSkillList = (name) => {
			for (let setSkill of setSkillList)
				if (name === setSkill.skill.name)
					return true
			return false
		}
		let skillList = []
		for (let name in skillDict)
			if (name !== "null" && !isInSetSkillList(name)) {
				// insertion sort
				let level = skillDict[name]
				for (var i = 0; i <= skillList.length; i++) {
					if (i === skillList.length) {
						skillList.push({name: name, level: level})
						break
					}
					if (skillList[i].level > level) {
						continue
					} else if (skillList[i].level < level) {
						skillList.splice(i, 0, {name: name, level: level})
						break
					} else {	// sort by levelMax
						if (LevelMax[skillList[i].name] > LevelMax[name])
							continue
						else if (LevelMax[skillList[i].name] < LevelMax[name]) {
							skillList.splice(i, 0, {name: name, level: level})
							break
						} else {	// sort by id, but temporary do the same thing as "<"
							skillList.splice(i, 0, {name: name, level: level})
							break
						}
					}
				}
			}
		this.setState({skillList: skillList})
		/*	todo:
			1. insertion sort with priority:
				(O) a. skill level
				b. skill id
			2. more functions: (in Drawer or in Modal?)
				a. clear armors
				b. clear slots
				c. import from MHW ArmorSearch (WebView)
				d. Settings
				e. output as a picture(armorset & skill level) and share
			3. long press ItemRow, and pop out itemInfo details. (Modal)
			4. Structure Issue:
				a. global.Refs ? props passing ?
				b. hashing object ? pass with sql query ? (depends on database schema)
			(OK) 5. setSkill

		main goal:
			(OK) A. Slots
			(OK) B. Weapons (customize slot)
			C. DB Manager
			D. SVG color masks & mappings
		*/
	}

	render() {		
		return (
			<View style={styles.container}>
				<ScrollView style={styles.skills}>
					{this.state.skillList.map((skill) => {
					let levelList = []
					for (var i = 0; i < 7; i++)
						levelList.push(i < skill.level ? 2 : i < LevelMax[skill.name] ? 1 : 0)
					return (
					<View key={skill.name}>
						<View style={styles.skill}>
							<View style={{width: 30, justifyContent: "center"}}>
								<View style={styles.icon}>
									<Image style={styles.image} source={require("../assets/png/skill/armor_skill.png")}/>
								</View>
							</View>
							<View style={styles.gap}/>
							<View style={styles.column}>								
								<TextContainer content={skill.name} align="flex-start"/>
								<View style={[styles.row, {paddingVertical: 3}]}>
									<View style={styles.icon}>
										<Image style={styles.image} source={require("../assets/png/skill/skill_left.png")}/>
									</View>
									{levelList.map((element, index) =>
									<View key={index} style={styles.icon}>
										{element === 2 ?
										<Image style={styles.image} source={require("../assets/png/skill/skill_filled.png")}/> :
										element === 1 ?
										<Image style={styles.image} source={require("../assets/png/skill/skill_empty.png")}/> :
										<Image style={styles.image} source={require("../assets/png/skill/skill_space.png")}/>}
									</View>)}
									<View style={styles.icon}>
										<Image style={styles.image} source={require("../assets/png/skill/skill_right.png")}/>
									</View>
								</View>
							</View>
						</View>
						<View style={styles.divider}/>
					</View>
					)})}
					{this.state.setSkillList.map((setSkill) => 
					<View key={setSkill.skill.name}>
						<View style={styles.setSkill}>
							<View style={{width: 30, justifyContent: "center"}}>
								<View style={styles.icon}>
									<Image style={styles.image} source={require("../assets/png/skill/armor_bonus_skill.png")}/>
								</View>
							</View>
							<View style={styles.gap}/>
							<View style={styles.column}>
								<TextContainer content={setSkill.name+"["+setSkill.skill.minLevel+"]"} align="flex-start"/>
								<TextContainer content={setSkill.skill.name} align="flex-start"/>
								<View style={styles.row}>
									{setSkill.parts.map((part) => {
									let image
									switch(part) {
										case "arm":		image = require("../assets/png/armor/arm.png");		break
										case "leg":		image = require("../assets/png/armor/leg.png");		break
										case "head":	image = require("../assets/png/armor/head.png");	break
										case "chest":	image = require("../assets/png/armor/chest.png");	break
										case "waist":	image = require("../assets/png/armor/waist.png");	break
										default:		image = require("../assets/png/armor/charm.png");
									}
									return (
										<View key={part} style={[styles.icon, {margin: 1}]}>
											<Image style={styles.image} source={image}/>
										</View>
									)})}
								</View>
							</View>
						</View>
						<View style={styles.divider}/>
					</View>)}
				</ScrollView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: "column",
		backgroundColor: "white",
	},
	skills: {
		flex: 1,
		flexDirection: "column",
	},
	row: {
		flex: 1,
		flexDirection: "row",
	},
	skill: {
		margin: 5,
		height: 42,
		flexDirection: "row",
	},
	setSkill: {
		margin: 5,
		height: 62,
		flexDirection: "row",
	},
	column: {
		flex: 1,
		flexDirection: "column",
	},
	icon: {
		aspectRatio: 1,
		alignItems: "center",
		justifyContent: "center",
	},
	image: {
		width: "100%",
		height: undefined,
		resizeMode: "contain",
		aspectRatio: 1,
	},
	divider: {
		height: 1,
		marginHorizontal: 5,
		backgroundColor: "gray",
	},
	gap: {
		width: 5,
	},
})
