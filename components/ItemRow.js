
import React from "react"
import {
	Text,
	View,
	Image,
	StyleSheet,
	TouchableOpacity,
} from "react-native"
import { Refs, LevelMax } from "./Global"
import TextContainer from "./TextContainer"

export default class ItemRow extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			part: this.props.part,
			itemInfo: this.props.itemInfo,
			currentSlots: [{name: null, skill: null}, {name: null, skill: null}, {name: null, skill: null}],
		}
	}

	setItemInfo(_itemInfo) {
		this.setState({itemInfo: _itemInfo})
		this.setState({currentSlots: [{name: null, skill: null}, {name: null, skill: null}, {name: null, skill: null}]})
	}

	setSlot(index, decoration) {
		let slots = this.state.currentSlots
		slots[index] = decoration
		this.setState({currentSlots: slots})
	}

	render() {
		let { navigate, goBack } = this.props.navigation
		let { itemInfo, currentSlots } = this.state
		let { part, isRemove, slotLock, slotInfo } = this.props		
		let { slots, rarity, defense, itemName, itemSkill } = itemInfo
		let image
		switch(part) {
			case "arm":		image = require("../assets/png/armor/arm.png");		break
			case "leg":		image = require("../assets/png/armor/leg.png");		break
			case "head":	image = require("../assets/png/armor/head.png");	break
			case "slot":
				if (isRemove) switch(slotInfo.slotLevel) {
					case 1:  image = require("../assets/png/slot/slot_lv1.png");	break
					case 2:  image = require("../assets/png/slot/slot_lv2.png");	break
					case 3:  image = require("../assets/png/slot/slot_lv3.png");	break
					default: image = require("../assets/png/slot/slots.png")
				} else switch(slotInfo.level) {
					case 1:  image = require("../assets/png/slot/decoration_lv1.png");	break
					case 2:  image = require("../assets/png/slot/decoration_lv2.png");	break
					case 3:  image = require("../assets/png/slot/decoration_lv3.png");	break
					default: image = require("../assets/png/slot/slots_white.png")
				}	break
			case "chest":	image = require("../assets/png/armor/chest.png");	break
			case "waist":	image = require("../assets/png/armor/waist.png");	break
			case "charm":	image = require("../assets/png/armor/charm.png");	break
			case "weapon":	image = require("../assets/png/weapon/lance.png");	break			
			default:		image = require("../assets/png/armor/armorset_empty.png")
		}
		return (
			<TouchableOpacity style={[styles.row, {margin: 5}]}
				onPress={slotLock || isRemove ? () => {
					new Promise((resolve, reject) => {
						if (part === "slot")
							Refs[slotInfo.base].current.setSlot(slotInfo.index, {name: itemName, skill: itemSkill[0].name})
						else	Refs[part].current.setItemInfo(itemInfo)
						resolve()	// callback func in "then"
					}).then(() => {
						Refs.drawer.current.updateSkills()
					})
					goBack()
				} : () => navigate("Search", {part: part})}>
				<View style={styles.rowIcon}>
					<Image style={styles.rowImage} source={image}/>
					<TextContainer content={"RARE "+rarity}/>
				</View>
				<View style={styles.gap}/>
				{isRemove ? <TextContainer content={part === "slot" ? "移除裝飾品" : "移除裝備"}/> :	
				itemName === null ? <TextContainer content={"新增裝備"}/> :
				<View style={styles.row}>
					{part === "weapon" ?
					<View style={styles.itemInfo}>
						<View style={styles.row}>
							<TextContainer style={styles.itemInfo1} content={itemName} align="flex-start"/>
							{defense.augMax === null ? <View style={styles.itemInfo2}/> : 
							<IconRow style={styles.itemInfo2} icon="defense" content={defense.max+"("+defense.augMax+")"}/>}
						</View>
						<View style={styles.row}>
							<IconRow style={styles.itemInfo1} icon="attack" content={itemSkill[0].name}/>
							<IconRow style={styles.itemInfo2} icon="affinity" content={itemSkill[0].level}/>
						</View>
						<View style={styles.row}>
							<IconRow style={styles.itemInfo1} icon="armor-skill" content={itemSkill[1].name}/>
							<IconRow style={styles.itemInfo2} icon="ice" content={itemSkill[1].level}/>
						</View>
					</View> :
					<View style={styles.itemInfo}>
						<View style={styles.row}>
							<TextContainer style={styles.itemInfo1} content={itemName} align="flex-start"/>
							{defense.augMax === null ? <View style={styles.itemInfo2}/> : 
							<IconRow style={styles.itemInfo2} icon="defense" content={defense.max+"("+defense.augMax+")"}/>}
						</View>
						<SkillRow itemSkill={itemSkill[0]}/>
						<SkillRow itemSkill={itemSkill[1]}/>						
					</View>}
					<View style={styles.itemSlots}>
						<SlotRow index={0} state={this.state} slotLock={slotLock} navigate={navigate}/>
						<SlotRow index={1} state={this.state} slotLock={slotLock} navigate={navigate}/>
						<SlotRow index={2} state={this.state} slotLock={slotLock} navigate={navigate}/>
					</View>
				</View>}
			</TouchableOpacity>
		)
	}
}

ItemRow.defaultProps = {
	isRemove: false,
	slotLock: false,
	itemInfo: {	
		slots: "0-0-0",
		rarity: "?",
		defense: {max: null, augMax: null},
		itemName: null,
		itemSkill: [{name: null, level: null}, {name: null, level: null}],
		setSkill: {name: null, minLevel: null},
	},
	navigation: null,
}

class SlotRow extends React.Component {
	render() {
		let { state, index, slotLock, navigate } = this.props
		let part = state.part
		let level = parseInt(state.itemInfo.slots[index*2])
		let content = state.currentSlots[index].name
		let image
		switch(level) {
			case 1:	image = require("../assets/png/slot/slot_lv1.png");	break
			case 2:	image = require("../assets/png/slot/slot_lv2.png");	break
			case 3:	image = require("../assets/png/slot/slot_lv3.png");	break
			default:	image = require("../assets/png/slot/slots.png")
		}
		return level === 0 ? <View style={styles.row}/> :
			<TouchableOpacity style={styles.row} disabled={slotLock}
				onPress={() => navigate("Search", {part: "slot", base: part, index: index, slotLevel: level})}>
				<View style={styles.icon}>
					<Image style={styles.image} source={image}/>
				</View>
				<View style={styles.gap}/>
				<TextContainer content={content === null ? "_____" : content} align="flex-start"/>
			</TouchableOpacity>
	}
}

class IconRow extends React.Component {
	render() {
		let { style, icon, content } = this.props
		let image
		switch(icon) {
			case "ice":			image = require("../assets/png/element/ice.png");		break
			case "blast":		image = require("../assets/png/element/blast.png");		break
			case "attack":		image = require("../assets/png/weapon/attack.png");		break
			case "defense":		image = require("../assets/png/weapon/defense.png");	break
			case "affinity":	image = require("../assets/png/weapon/affinity.png");	break
			case "armor-skill":	image = require("../assets/png/skill/armor_skill.png");	break
			default:			image = require("../assets/png/skill/armor_skill.png");
		}
		return content === null ? <View style={style}/> :			
			<View style={style}>
				<View style={styles.icon}>
					<Image style={styles.image} source={image}/>
				</View>
				<View style={styles.gap}/>
				<TextContainer content={content} align="flex-start"/>
			</View>
	}
}

class SkillRow extends React.Component {
	render() {
		let { name, level } = this.props.itemSkill
		let levelList = []
		for (var i = 0; i < 3; i++)
			levelList.push(i < level ? 2 : i < LevelMax[name] ? 1 : 0)		
		return name === null ? <View style={styles.row}/> :
			<View style={styles.row}>
				<View style={styles.itemInfo1}>
					<View style={styles.icon}>
						<Image style={styles.image} source={require("../assets/png/skill/armor_skill.png")}/>
					</View>
					<View style={styles.gap}/>
					<TextContainer content={name} align="flex-start"/>
				</View>
				<View style={[styles.itemInfo2, {paddingVertical: 2}]}>
					<View style={styles.icon}>
						<Image style={styles.image} source={require("../assets/png/skill/skill_left.png")}/>
					</View>
					{levelList.map((element, index) =>
					<View key={index} style={styles.icon}>
						{element === 2 ?
						<Image style={styles.image} source={require("../assets/png/skill/skill_filled.png")}/> :
						element === 1 ?
						<Image style={styles.image} source={require("../assets/png/skill/skill_empty.png")}/> :
						<Image style={styles.image} source={require("../assets/png/skill/skill_space.png")}/>}
					</View>)}
					<View style={styles.icon}>
						<Image style={styles.image} source={require("../assets/png/skill/skill_right.png")}/>
					</View>
				</View>
			</View>
	}	
}

const styles = StyleSheet.create({
	row: {
		flex: 1,
		flexDirection: "row",
	},
	column: {
		flex: 1,
		flexDirection: "column",
	},
	rowIcon: {
		padding: 3,
		borderWidth: 1,
		borderRadius: 5,
		borderColor: "gray",

		aspectRatio: 1,
		alignItems: "center",
		justifyContent: "center",
	},
	rowImage: {
		width: "80%",
		height: undefined,
		resizeMode: "contain",
		aspectRatio: 1,
		marginBottom: 3,
	},
	// ratio between the three columns: 11:8:6
	itemInfo: {
		flex: 19,
		flexDirection: "column",
	},
	itemInfo1: {
		flex: 11,
		flexDirection: "row",
	},
	itemInfo2: {
		flex: 8,
		flexDirection: "row",
	},
	itemSlots: {
		flex: 6,
		flexDirection: "column",
	},
	// ratio between the three columns: 11:8:6
	icon: {
		aspectRatio: 1,
		alignItems: "center",
		justifyContent: "center",
	},
	image: {
		width: "100%",
		height: undefined,
		resizeMode: "contain",
		aspectRatio: 1,		
	},
	gap: {
		width: 5,
	},
})
