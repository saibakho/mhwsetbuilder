import React from "react"

var Refs = {
	weapon: React.createRef(),
	head: React.createRef(),
	chest: React.createRef(),
	arm: React.createRef(),
	waist: React.createRef(),
	leg: React.createRef(),
	charm: React.createRef(),
	drawer: React.createRef(),
}

const SlotLevel = {
	"超心珠": 2,
	"痛擊珠": 2,
	"心眼珠": 2,
}

const SlotSkill = {
	"超心珠": "超會心",
	"痛擊珠": "弱點特效",
	"心眼珠": "心眼／彈道強化",
}

const LevelMax = {
	"匠": 5,
	"耳塞": 5,
	"挑戰者": 5,
	"迴避性能": 5,
	"雷屬性攻擊強化": 5,

	"跑者": 3,
	"整備": 3,
	"無傷": 3,
	"體力增強": 3,
	"迴避距離UP": 3,
	"屬性異常耐性": 3,
	"屬性解放／裝填擴充": 3,

	"突破耐力上限": 1,
	"心眼／彈道強化": 1,
	"會心攻擊[屬性]": 1,
}

export { Refs, SlotLevel, SlotSkill, LevelMax }
